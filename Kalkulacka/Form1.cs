﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulacka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string prevEquation = "", prevOperation = "", operation = "", lastResult = "";

        double answer = 0;

        bool changeDisplay = false;
        
        private void AllBtn_Click(object sender, EventArgs e)
        {
                      
            Button btn = sender as Button;
             
            switch (btn.Name)
            {
                case "btnHelp":
                    MessageBox.Show
                        (
                           "AC - zmaže zadané čísla aj operátory ak sú polia prázdne zmaže uložené výsledky. " +
                           "CE - zmaže celé práve zadávane číslo. " +
                           "DEL - zmaže poslednú zadanú číslovku. "                        
                        );
                    break;
                 
                case "btnDel":
                    if (txtDisplay.Text.Length > 0)
                    {
                        txtDisplay.Text = txtDisplay.Text.Substring(0, txtDisplay.Text.Length - 1);
                    }
                    break;
                
                case "btnClearAll":
                    if (txtDisplay.Text.Length > 0 || txtDisplay2.Text.Length > 0)
                    {
                        operation = "";
                        txtDisplay.ResetText();
                        txtDisplay2.ResetText();
                    }
                    else if(txtDisplay.Text.Length == 0 && txtDisplay2.Text.Length == 0)
                    {
                        operation = "";
                        txtDisplay3.ResetText();
                        txtDisplay4.ResetText();
                    }
                    break;

                case "btnCE":
                    txtDisplay.ResetText();
                    break;
                
                case "btnDecimal":
                    if (!txtDisplay.Text.Contains(",") && txtDisplay.Text.Length > 0)
                    {
                        txtDisplay.Text += ",";
                    }
                    break;

                case "btnNegation":
                    if (txtDisplay.Text.Length > 0)
                    {
                        if (!txtDisplay.Text.Contains("-"))
                        {
                            txtDisplay.Text = "-" + txtDisplay.Text;
                        }
                        else if (txtDisplay.Text.Contains("-"))
                        {
                            txtDisplay.Text = txtDisplay.Text.Substring(1, txtDisplay.Text.Length - 1);
                        }
                    }
                    break;

                default:
                    if (operation == "=")
                    {
                        operation = "";
                        txtDisplay.ResetText();
                    }
                    if(txtDisplay.Text.Length <= 14)
                    {
                        txtDisplay.Text += btn.Text;
                    }
                    break;
            }
        }

        private void Operation_Click(object sender, EventArgs e)
        {
            Button opr = sender as Button;

            switch (opr.Text)
            {
                case "+":
                    if (txtDisplay.Text.Length > 0 && txtDisplay.Text.Substring(0, txtDisplay.Text.Length - 1) != "+")
                    {
                        if (operation == "" || operation == "=")
                        {
                            operation = "+";
                            prevOperation = operation;
                            prevEquation = txtDisplay.Text;
                            txtDisplay2.Text = prevEquation + operation;
                            txtDisplay.ResetText();
                        }
                        else if (operation == "+" || prevOperation == "÷" || prevOperation == "X" || prevOperation == "-")
                        {
                            operation = "=";
                            multi_equations();
                            txtDisplay2.ResetText();
                            txtDisplay.Text = answer.ToString();
                            if (txtDisplay3.Text.Length > 0 || changeDisplay == true)
                            {
                                txtDisplay4.Text = txtDisplay3.Text;
                                txtDisplay3.Text = lastResult;
                                changeDisplay = false;
                            }
                            else if (changeDisplay == false)
                            {
                                txtDisplay3.Text = lastResult;
                                changeDisplay = true;
                            }

                            operation = "+";
                            prevOperation = operation;
                            prevEquation = txtDisplay.Text;
                            txtDisplay2.Text = prevEquation + operation;
                            txtDisplay.ResetText();
                        }
                    }
                    break;
                case "-":
                    if (txtDisplay.Text.Length > 0 && txtDisplay.Text.Substring(0, txtDisplay.Text.Length - 1) != "-")
                    {
                        if (operation == "" || operation == "=")
                        {
                            operation = "-";
                            prevOperation = operation;
                            prevEquation = txtDisplay.Text;
                            txtDisplay2.Text = prevEquation + operation;
                            txtDisplay.ResetText();
                        }
                        else if (operation == "-" || prevOperation == "÷" || prevOperation == "+" ||prevOperation == "X")
                        {
                            operation = "=";
                            multi_equations();
                            txtDisplay2.ResetText();
                            txtDisplay.Text = answer.ToString();
                            if (txtDisplay3.Text.Length > 0 || changeDisplay == true)
                            {
                                txtDisplay4.Text = txtDisplay3.Text;
                                txtDisplay3.Text = lastResult;
                                changeDisplay = false;
                            }
                            else if (changeDisplay == false)
                            {
                                txtDisplay3.Text = lastResult;
                                changeDisplay = true;
                            }

                            operation = "-";
                            prevOperation = operation;
                            prevEquation = txtDisplay.Text;
                            txtDisplay2.Text = prevEquation + operation;
                            txtDisplay.ResetText();
                        }
                    }
                    break;
                case "÷":
                    if (txtDisplay.Text.Length > 0 && txtDisplay.Text.Substring(0, txtDisplay.Text.Length - 1) != "÷")
                    {
                        if (operation == "" || operation == "=")
                        {
                            operation = "÷";
                            prevOperation = operation;
                            prevEquation = txtDisplay.Text;
                            txtDisplay2.Text = prevEquation + operation;
                            txtDisplay.ResetText();   
                        }
                        else if (operation == "÷" || prevOperation == "X" || prevOperation == "+" || prevOperation == "-")
                        {
                            operation = "=";
                            multi_equations();
                            txtDisplay2.ResetText();
                            txtDisplay.Text = answer.ToString();
                            if (txtDisplay3.Text.Length > 0 || changeDisplay == true)
                            {
                                txtDisplay4.Text = txtDisplay3.Text;
                                txtDisplay3.Text = lastResult;
                                changeDisplay = false;
                            }
                            else if (changeDisplay == false)
                            {
                                txtDisplay3.Text = lastResult;
                                changeDisplay = true;
                            }

                            operation = "÷";
                            prevOperation = operation;
                            prevEquation = txtDisplay.Text;
                            txtDisplay2.Text = prevEquation + operation;
                            txtDisplay.ResetText();
                        }
                    }
                    break;
                case "X":
                    if (txtDisplay.Text.Length > 0 && txtDisplay.Text.Substring(0, txtDisplay.Text.Length - 1) != "X")
                    {
                        if (operation == "" || operation == "=")
                        {
                            operation = "X";
                            prevOperation = operation;
                            prevEquation = txtDisplay.Text;
                            txtDisplay2.Text = prevEquation + operation;
                            txtDisplay.ResetText(); 
                        }
                        else if (operation == "X" || prevOperation == "÷" || prevOperation == "+" || prevOperation == "-" )
                        {
                            operation = "=";
                            multi_equations();
                            txtDisplay2.ResetText();
                            txtDisplay.Text = answer.ToString();
                            if (txtDisplay3.Text.Length > 0 || changeDisplay == true)
                            {
                                txtDisplay4.Text = txtDisplay3.Text;
                                txtDisplay3.Text = lastResult;
                                changeDisplay = false;
                            }
                            else if (changeDisplay == false)
                            {
                                txtDisplay3.Text = lastResult;
                                changeDisplay = true;
                            }
                            operation = "X";
                            prevOperation = operation;
                            prevEquation = txtDisplay.Text;
                            txtDisplay2.Text = prevEquation + operation;
                            txtDisplay.ResetText();
                        }
                    }
                    break;
                case "=":
                    if (txtDisplay.Text.Length > 0)
                    {
                        operation = "=";
                        multi_equations();
                        txtDisplay2.ResetText();
                        txtDisplay.Text = answer.ToString();
                        if (txtDisplay3.Text.Length > 0 || changeDisplay == true)
                        {
                            txtDisplay4.Text = txtDisplay3.Text;
                            txtDisplay3.Text = lastResult;
                            changeDisplay = false;
                        }
                        else if (changeDisplay == false)
                        {
                            txtDisplay3.Text = lastResult;
                            changeDisplay = true;
                        }
                    }
                    break;
            }
        }

        private void multi_equations()
        {
            if (prevOperation == "+")
            {
                prevOperation = operation;

                answer = Convert.ToDouble(prevEquation) + Convert.ToDouble(txtDisplay.Text);

                string firstNumber = "";

                firstNumber = prevEquation;
                prevEquation = answer.ToString();
                txtDisplay2.Text = prevEquation + operation;
                lastResult = firstNumber + " + " + txtDisplay.Text + " = " + txtDisplay2.Text.Substring(0, txtDisplay2.Text.Length - 1);
                txtDisplay.ResetText();
            }
            else if (prevOperation == "-")
            {
                prevOperation = operation;

                answer = Convert.ToDouble(prevEquation) - Convert.ToDouble(txtDisplay.Text);

                string firstNumber = "";

                firstNumber = prevEquation;
                prevEquation = answer.ToString();
                txtDisplay2.Text = prevEquation + operation;
                lastResult = firstNumber + " - " + txtDisplay.Text + " = " + txtDisplay2.Text.Substring(0, txtDisplay2.Text.Length - 1);
                txtDisplay.ResetText();
            }
            else if (prevOperation == "÷")
            {
                prevOperation = operation;
                
                answer = Convert.ToDouble(prevEquation) / Convert.ToDouble(txtDisplay.Text);

                string firstNumber = "";

                firstNumber = prevEquation;
                prevEquation = answer.ToString();
                txtDisplay2.Text = prevEquation + operation;
                lastResult = firstNumber + " ÷ " + txtDisplay.Text + " = " + txtDisplay2.Text.Substring(0, txtDisplay2.Text.Length - 1);
                txtDisplay.ResetText();
            }
            else if (prevOperation == "X")
            {
                prevOperation = operation;

                answer = Convert.ToDouble(prevEquation) * Convert.ToDouble(txtDisplay.Text);

                string firstNumber = "";

                firstNumber = prevEquation;
                prevEquation = answer.ToString();
                txtDisplay2.Text = prevEquation + operation;
                lastResult = firstNumber + " X " + txtDisplay.Text + " = " + txtDisplay2.Text.Substring(0, txtDisplay2.Text.Length - 1);
                txtDisplay.ResetText();
            }
        }
    }
}